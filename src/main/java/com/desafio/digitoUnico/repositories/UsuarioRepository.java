package com.desafio.digitoUnico.repositories;

import com.desafio.digitoUnico.domains.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

//    @Override
//    public List<Usuario> findAll();

//    @Override
//    @Query("SELECT * FROM TBN_USUARIO where id = (:id)")
//     Optional<Usuario> findById(@Param("id") Long id);

}
