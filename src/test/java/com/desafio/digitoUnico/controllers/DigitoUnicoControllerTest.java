package com.desafio.digitoUnico.controllers;

import com.desafio.digitoUnico.domains.DigitoUnico;
import com.desafio.digitoUnico.dtos.DigitoUnicoCalculoDto;
import com.desafio.digitoUnico.dtos.DigitoUnicoDto;
import com.desafio.digitoUnico.dtos.UsuarioDto;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import com.desafio.digitoUnico.controllers.DigitoUnicoController;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(DigitoUnicoController.class)
@RunWith(SpringRunner.class)
class DigitoUnicoControllerTest {

    private static final Long ID = 1L;

    @MockBean
    private DigitoUnicoController controller;

    @Autowired
    private MockMvc mvc;

     @Test
    void addDigitoUnico() throws Exception {
         DigitoUnicoCalculoDto dto = new DigitoUnicoCalculoDto();
        dto.setNumeroConcatenacao(2);
        dto.setDigito("1621");
        dto.setIdUsuario(1L);

         DigitoUnicoDto dtos = new DigitoUnicoDto();
        given(controller.addDigitoUnico(dto)).willReturn(new ResponseEntity<DigitoUnicoDto>(dtos, HttpStatus.CREATED));

        MvcResult result =  mvc.perform(post("http://localhost:8080/api/DigitoUnico/Cadastrar").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn();
        String content = result.getResponse().getContentAsString();
    }

    @Test
    void getAllByUsuario() throws Exception {
        DigitoUnico dto =new DigitoUnico();
        List<DigitoUnico> dtos = Arrays.asList(dto);

        given(controller.getAllByUsuario(ID)).willReturn( new ResponseEntity<List<DigitoUnico>>(dtos, HttpStatus.OK));
        MvcResult result = mvc.perform(get("http://localhost:8080/api/usuario/getAllByUsuario/"+ ID).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn();
    }
}