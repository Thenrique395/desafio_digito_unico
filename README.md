# Desafio Dígito Único

## Sobre o projeto

Uma API que recebe um número inteiro e calcula um dígito único para esse número. A função que calcula o dígito único recebe dois parâmetros:

**n**: número inteiro

**k**: número inteiro que representa o número de vezes que *n* será repetido

*Exemplo:*

```
n = 123, k = 2
digitoUnico(n, k) = 123123
somarDigitos(123123) = 1 + 2 + 3 + 1 + 2 + 3 = 12
```

Se o dígito encontrado ainda não for único, a operação de soma se repete

```
somarDigitos(12) = 1 + 2 = 3
```

O projeto possui endpoints para CRUD de Usuário e Dígitos Unicos.

### Tecnologias utilizadas

- Java 8
- SpringBoot
- H2 Database
- Maven

### Ferramentas utilizadas

- Lombok 
- Swagger 
- Postman 
- Git 


### Teste

Para testar a aplicação, seguir o seguinte caminho:

```
src > test > java > com.desafio.digitoUnico DigitoUnicoApplicationTests
```

### Instruções para a execução

O projeto conta com uma classe "DigitoUnicoApplication" dentro da API. Para startar a aplicação SpringBoot.

```
digitoUnico > src > main > java > com.desafio.digitounico > DigitoUnicoApplication
```
via CMD

```bash
java -jar digitoUnico.jar
```

### Paths e endpoints

Path original da API:  [digitoUnico-API](http://localhost:8080/api)

Endpoints: [Swagger UI]("http://localhost:8080/swagger-ui.html")

### Contribuição

Alterações e novas melhorias fique a vontade para criar novos pull request.

### Contatos: 


Telefone : (81)99525-7823

Linkedin : [Tiago Henrique ]("https://www.linkedin.com/in/tiago-henrique-dos-santos-083979101/")

E-mail : "Thenrique395@gmail.com"
