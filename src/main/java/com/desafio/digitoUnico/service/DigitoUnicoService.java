package com.desafio.digitoUnico.service;

import com.desafio.digitoUnico.config.CacheConfig;
import com.desafio.digitoUnico.domains.DigitoUnico;
import com.desafio.digitoUnico.dtos.DigitoUnicoCalculoDto;
import com.desafio.digitoUnico.dtos.DigitoUnicoDto;
import com.desafio.digitoUnico.repositories.DigitoUnicoRepository;
import com.desafio.digitoUnico.utils.CalcularUtils;
import com.desafio.digitoUnico.utils.ConversorDigitoUnico;
import com.desafio.digitoUnico.utils.ConversorUsuario;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

//@Slf4j
@Service
//@Transactional(propagation = Propagation.REQUIRED)
public class DigitoUnicoService {

    private DigitoUnicoRepository digitoUnicoRepository;
    private UsuarioService usuarioService;
    private ConversorDigitoUnico conversorDigitoUnico;

    @Autowired
    public  DigitoUnicoService(DigitoUnicoRepository digitoUnicoRepository, UsuarioService usuarioService, ConversorDigitoUnico conversorDigitoUnico){
        this.digitoUnicoRepository = digitoUnicoRepository;
        this.usuarioService = usuarioService;
        this.conversorDigitoUnico = conversorDigitoUnico;
    }

    public DigitoUnicoDto CadastrarDigitoUnico(DigitoUnicoCalculoDto digitoUnicoCalculoDto) {
        DigitoUnico digitoUnico = conversorDigitoUnico.convertToEntity(digitoUnicoCalculoDto);
        Integer calcular = CalcularUtils.calcular(digitoUnicoCalculoDto.getDigito(), digitoUnicoCalculoDto.getNumeroConcatenacao());
        digitoUnico.setNumeroEncontrado(calcular);
        DigitoUnico digitoUnicoCadastrado = digitoUnicoRepository.save(digitoUnico);
        DigitoUnicoDto digitoUnicodto = conversorDigitoUnico.convertToDTO(digitoUnicoCadastrado);

        return digitoUnicodto;
    }

    public Integer calcular(DigitoUnicoCalculoDto paramDto) {
        Integer digitoUnico = CacheConfig.buscar(paramDto.getDigito(), paramDto.getNumeroConcatenacao());
        if (Objects.isNull(digitoUnico)) {
            digitoUnico = CalcularUtils.calcular(paramDto.getDigito(), paramDto.getNumeroConcatenacao());
            CacheConfig.adicionar(paramDto.getDigito(), paramDto.getNumeroConcatenacao(), digitoUnico);
        }
        return digitoUnico;
    }



    public List<DigitoUnico> digitoUnicoByUsuario(Long id) {
        List<DigitoUnico> allByUsuario = digitoUnicoRepository.getAllByUsuario(id);
       // DigitoUnicoDto digitoUnicodto = conversorDigitoUnico.convertToDTO(allByUsuario);

        return allByUsuario;
    }
}
