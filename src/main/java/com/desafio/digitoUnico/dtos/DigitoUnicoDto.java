package com.desafio.digitoUnico.dtos;


import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DigitoUnicoDto {

    private Long id;
    private String digito;
    private Integer numeroConcatenacao;
    private Integer numeroEncontrado;
    private Long idusuario    ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDigito() {
        return digito;
    }

    public void setDigito(String digito) {
        this.digito = digito;
    }

    public Integer getNumeroConcatenacao() {
        return numeroConcatenacao;
    }

    public void setNumeroConcatenacao(Integer numeroConcatenacao) {
        this.numeroConcatenacao = numeroConcatenacao;
    }

    public Integer getNumeroEncontrado() {
        return numeroEncontrado;
    }

    public void setNumeroEncontrado(Integer numeroEncontrado) {
        this.numeroEncontrado = numeroEncontrado;
    }

    public Long getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(Long idusuario) {
        this.idusuario = idusuario;
    }


}
