package com.desafio.digitoUnico.utils;

import com.desafio.digitoUnico.domains.DigitoUnico;
import com.desafio.digitoUnico.domains.Usuario;
import com.desafio.digitoUnico.dtos.UsuarioDto;
import com.desafio.digitoUnico.repositories.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
 public class ConversorUsuario implements converter<Usuario , UsuarioDto> {

    @Autowired
    UsuarioRepository usuarioRepository;

    public  Usuario convertToEntity(UsuarioDto dto) {
        Usuario entity = new Usuario();
        BeanUtils.copyProperties(dto, entity);
        return entity;
    }


    public UsuarioDto convertToDTO(Usuario entity) {
        UsuarioDto dto = new UsuarioDto();
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

}
