package com.desafio.digitoUnico.utils;

import com.desafio.digitoUnico.domains.DigitoUnico;
import com.desafio.digitoUnico.domains.Usuario;
import com.desafio.digitoUnico.dtos.DigitoUnicoCalculoDto;
import com.desafio.digitoUnico.dtos.DigitoUnicoDto;
import com.desafio.digitoUnico.repositories.UsuarioRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;

@Component
public class ConversorDigitoUnico {

    @Autowired
    UsuarioRepository usuarioRepository;

    public DigitoUnico convertToEntity(DigitoUnicoDto digitoUnicoDto) {
        DigitoUnico entity = new DigitoUnico();
        if (Objects.nonNull(digitoUnicoDto.getIdusuario())) {
            Usuario usuario = usuarioRepository.getOne(digitoUnicoDto.getIdusuario());
            entity.setUsuario(usuario);
        }
        BeanUtils.copyProperties(entity, digitoUnicoDto);
        return entity;
    }

    public DigitoUnico convertToEntity(DigitoUnicoCalculoDto digitoUnicoDto) {
        DigitoUnico entity = new DigitoUnico();
        if (Objects.nonNull(digitoUnicoDto.getIdUsuario())) {
            Usuario usuario = usuarioRepository.findById(digitoUnicoDto.getIdUsuario()).get();
            entity.setUsuario(usuario);
        }
        BeanUtils.copyProperties( digitoUnicoDto,entity);

        return entity;
    }

    public DigitoUnicoDto convertToDTO(DigitoUnico entity) {
        DigitoUnicoDto dto = new DigitoUnicoDto();
        if (Objects.nonNull(entity.getUsuario())) {
            dto.setIdusuario(entity.getUsuario().getId());
        }
        BeanUtils.copyProperties( entity,dto);

        return dto;
    }
}

