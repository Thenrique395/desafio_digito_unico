package com.desafio.digitoUnico.repositories;

import com.desafio.digitoUnico.domains.DigitoUnico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico,Long> {

    @Query(value = "SELECT * FROM TBN_DIGITO_UNICO  WHERE  IDUSUARIO = (:IDusuario)", nativeQuery = true)
    public List<DigitoUnico> getAllByUsuario(@Param("IDusuario") Long IDusuario);


}
