package com.desafio.digitoUnico.controllers;

import com.desafio.digitoUnico.domains.DigitoUnico;
import com.desafio.digitoUnico.dtos.DigitoUnicoCalculoDto;
import com.desafio.digitoUnico.dtos.DigitoUnicoDto;
import com.desafio.digitoUnico.service.DigitoUnicoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Api("DigitoUnico")
@RequestMapping("/api/DigitoUnico")
public class DigitoUnicoController {

    private DigitoUnicoService digitoUnicoService;

    @Autowired
    public DigitoUnicoController(DigitoUnicoService digitoUnicoService) {
        this.digitoUnicoService = digitoUnicoService;
    }

    @PostMapping("/Cadastrar")
    @ApiOperation(httpMethod = "POST", value = "Cadastra digitos para o usuário")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "digitos cadastrado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao cadastrar digito") })
    public ResponseEntity<DigitoUnicoDto> addDigitoUnico(@RequestBody DigitoUnicoCalculoDto digitoUnicoCalculoDto) {
        DigitoUnicoDto digitoUnicoDto = digitoUnicoService.CadastrarDigitoUnico(digitoUnicoCalculoDto);

        return  new ResponseEntity(digitoUnicoDto, HttpStatus.ACCEPTED);
    }


    //Falta implementar
    @PostMapping("/Calcular")
    @ApiOperation(httpMethod = "POST", value = "Calcula os digitos no cache")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "digitos cadastrado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao cadastrar digito") })
    public ResponseEntity<List<DigitoUnicoDto>> Calcular(@RequestBody DigitoUnicoCalculoDto digitoUnicoCalculoDto) {
        Integer calcular = digitoUnicoService.calcular(digitoUnicoCalculoDto);

        return  new ResponseEntity(calcular, HttpStatus.OK);
    }

    @GetMapping("/Digitounico/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "GET", value = "Listar todos os dígitos de um usuário específico")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista de dígitos"),
            @ApiResponse(code = 204, message = "Nenhum dígito encontrado!"),
            @ApiResponse(code = 400, message = "Erro!")})
    public ResponseEntity<List<DigitoUnico>> getAllByUsuario(@PathVariable(value = "id") Long id) {
        List<DigitoUnico> digitoUnicos = digitoUnicoService.digitoUnicoByUsuario(id);

        return  new ResponseEntity<List<DigitoUnico>>(digitoUnicos, HttpStatus.OK);
    }


}
