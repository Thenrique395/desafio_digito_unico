package com.desafio.digitoUnico.config;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;
public class CacheConfig {

    private static LinkedHashMap<String, Integer> Linkedcache = new LinkedHashMap<>();

    private CacheConfig() {
        throw new IllegalStateException("Utility class");
    }

    public static void adicionar(String digito, Integer concatenacao, Integer digitoUnico) {
        if (Linkedcache.size() == 10) {
            removerChaveAntiga();
        }

        String chave = gerarNovaChave(digito, concatenacao);
        if (!procurarChave(chave)) {
            Linkedcache.put(chave, digitoUnico);
        }
    }

    public static Integer buscar(String digito, Integer concatenacao) {
        Integer digitoUnico = null;
        String chave = gerarNovaChave(digito, concatenacao);
        if (procurarChave(chave)) {
            digitoUnico = Linkedcache.get(chave);
        }
        return digitoUnico;
    }

    private static String gerarNovaChave(String digito, Integer concatenacao) {
        return digito.concat("-").concat(String.valueOf(concatenacao));
    }

    private static void removerChaveAntiga() {
        String chaveAntiga = Linkedcache.keySet().stream().findFirst().orElse(null);
        Linkedcache.remove(chaveAntiga);
    }

    private static boolean procurarChave(String chave) {
        return Linkedcache.containsKey(chave);
    }

    public static Set<Entry<String, Integer>> getCache() {
        return Linkedcache.entrySet();
    }
}
