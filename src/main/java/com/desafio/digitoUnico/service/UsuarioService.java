package com.desafio.digitoUnico.service;

import com.desafio.digitoUnico.domains.Usuario;
import com.desafio.digitoUnico.dtos.UsuarioDto;
import com.desafio.digitoUnico.repositories.UsuarioRepository;
import com.desafio.digitoUnico.utils.ConversorUsuario;
import com.desafio.digitoUnico.utils.CriptografiaUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.Null;
import java.security.GeneralSecurityException;
import java.util.Optional;

@Slf4j
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UsuarioService {

    private UsuarioRepository usuarioRepository;
    private ConversorUsuario conversorUsuario;

    @Autowired
    public UsuarioService(UsuarioRepository usuarioRepository, ConversorUsuario conversorUsuario) {
        this.usuarioRepository = usuarioRepository;
        this.conversorUsuario = conversorUsuario;
    }

    public UsuarioDto cadastrar(@Valid @RequestBody UsuarioDto usuarioDto) {
        Usuario usuario = conversorUsuario.convertToEntity(usuarioDto);

        @Valid Usuario usuarioCadastrado = usuarioRepository.save(usuario);

        UsuarioDto entityDto = conversorUsuario.convertToDTO(usuarioCadastrado);

        return entityDto;
    }

    public UsuarioDto getById(Long id) {
        Optional<Usuario> usuarioCadastrado = usuarioRepository.findById(id);
        UsuarioDto entityDto = conversorUsuario.convertToDTO(usuarioCadastrado.get());
        return entityDto;
    }

    public UsuarioDto delete(Long id) {
        UsuarioDto usuarioDto = getById(id);
        Usuario usuario = conversorUsuario.convertToEntity(usuarioDto);
        usuarioRepository.delete(usuario);

        return usuarioDto;
    }

    public UsuarioDto atualizar(UsuarioDto usuarioDto) {

        Usuario usuarioAtualizado = new Usuario();

    if (getById(usuarioDto.getId()) != null) {
            Usuario usuario = conversorUsuario.convertToEntity(usuarioDto);
            usuarioAtualizado = usuarioRepository.saveAndFlush(usuario);
        }
        UsuarioDto entityDto = conversorUsuario.convertToDTO(usuarioAtualizado);

        return entityDto;
    }

    public UsuarioDto criptografar(Long id) throws GeneralSecurityException {
        Usuario usuarioCarregado = usuarioRepository.findById(id).orElse(null);
        Usuario usuarioCriptografado =  CriptografiaUtils.criptografar(usuarioCarregado);
        Usuario usuario = usuarioRepository.save(usuarioCriptografado);
        UsuarioDto dto = conversorUsuario.convertToDTO(usuario);
        return dto;
    }

    public UsuarioDto descriptografar(Long id) {
        Usuario usuarioCarregado = usuarioRepository.findById(id).orElse(null);
        Usuario usuarioDescriptografado = CriptografiaUtils.descriptografar(usuarioCarregado);
        Usuario usuario = usuarioRepository.save(usuarioDescriptografado);
        UsuarioDto dto = conversorUsuario.convertToDTO(usuario);
        return dto;
    }

}
