package com.desafio.digitoUnico.dtos;

import lombok.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UsuarioDigitoDto {

    private Long id;
    private String nome;
    private String email;
    @Builder.Default
    private List<DigitoUnicoDto> digitos = new ArrayList<>();

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getNome() {
//        return nome;
//    }
//
//    public void setNome(String nome) {
//        this.nome = nome;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public List<DigitoUnicoDto> getDigitos() {
//        return digitos;
//    }
//
//    public void setDigitos(List<DigitoUnicoDto> digitos) {
//        this.digitos = digitos;
//    }
}