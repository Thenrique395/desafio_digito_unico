package com.desafio.digitoUnico.utils;

public  interface converter<T,Dto> {

     T convertToEntity(Dto dto);

    Dto convertToDTO(T entity);


}
