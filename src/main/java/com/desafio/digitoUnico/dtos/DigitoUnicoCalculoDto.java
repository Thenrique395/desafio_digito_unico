package com.desafio.digitoUnico.dtos;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DigitoUnicoCalculoDto {

    private Long idUsuario;
    private String digito;
    private Integer numeroConcatenacao;

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getDigito() {
        return digito;
    }

    public void setDigito(String digito) {
        this.digito = digito;
    }

    public Integer getNumeroConcatenacao() {
        return numeroConcatenacao;
    }

    public void setNumeroConcatenacao(Integer numeroConcatenacao) {
        this.numeroConcatenacao = numeroConcatenacao;
    }
}
