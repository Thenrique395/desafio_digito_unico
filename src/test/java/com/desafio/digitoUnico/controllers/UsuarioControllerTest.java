package com.desafio.digitoUnico.controllers;

import com.desafio.digitoUnico.controllers.*;
import com.desafio.digitoUnico.dtos.UsuarioDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UsuarioController.class)
@RunWith(SpringRunner.class)
public class UsuarioControllerTest {

    private static final Long ID = 1L;
    private static final String NOME = "Tiago Henrique";
    private static final String EMAIL = "thenrique395@gmail.com";
    private static final ObjectMapper OM = new ObjectMapper();

    @MockBean
    private UsuarioController controller;

    @Autowired
    private MockMvc mvc;

    @Test
    public void getById() throws Exception {
        UsuarioDto dto =new UsuarioDto();
        List<UsuarioDto> dtos = Arrays.asList(dto);

        given(controller.getById(ID)).willReturn( new ResponseEntity<UsuarioDto>(dto, HttpStatus.CREATED));
        MvcResult result = mvc.perform(get("http://localhost:8080/api/usuario/getById/"+ ID).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().is(201)).andReturn();
    }

    @Test
    public void create() throws Exception {
        UsuarioDto dto = new UsuarioDto();
        dto.setNome(NOME);
        dto.setEmail(EMAIL);
        dto.setId(1L);

        UsuarioDto dtos = new UsuarioDto();
        given(controller.cadastrar(dto)).willReturn(new ResponseEntity<>(dtos, HttpStatus.CREATED));

        MvcResult result =  mvc.perform(post("http://localhost:8080/api/usuario/cadastrar").contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)).andReturn();//.andExpect(status().is(400)).andReturn();
        String content = result.getResponse().getContentAsString();
    }

    @Test
    public void criptografar() throws Exception {
        UsuarioDto dto =new UsuarioDto();

        given(controller.criptografarUsuario(ID)).willReturn( new ResponseEntity(dto, HttpStatus.OK));
        MvcResult result = mvc.perform(get("http://localhost:8080/api/usuario/criptografar/"+ ID).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn();//.andExpect(status().is(201)).andReturn();
    }

    @Test
    public void descriptografar() throws Exception {
        UsuarioDto dto =new UsuarioDto();

        given(controller.criptografarUsuario(ID)).willReturn( new ResponseEntity(dto, HttpStatus.OK));
        MvcResult result = mvc.perform(get("http://localhost:8080/api/usuario/descriptografar/"+ ID).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn();//.andExpect(status().is(201)).andReturn();
    }


    @Test
    public void update() throws Exception {
        UsuarioDto dto = new UsuarioDto();
        dto.setNome(NOME);
        dto.setEmail(EMAIL);
        dto.setId(1L);
        UsuarioDto dtos = new UsuarioDto();

        when(controller.atualizar(dto)).thenReturn(new ResponseEntity<>(dtos, HttpStatus.CREATED));
        MvcResult result =  mvc.perform(post("http://localhost:8080/api/usuario/atualizar",dto).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andReturn();//.andExpect(status().is(404)).andReturn();
    }

    @Test
    public void delete() throws Exception {
        given(controller.remover(ID)).willReturn(new ResponseEntity(ID, HttpStatus.OK));

      //  ResultActions perform = mvc.perform(delete("http://localhost:8080/api/usuario/delete/1").accept(MediaType.APPLICATION_JSON).andExpect(status().is(201));

    }

}
