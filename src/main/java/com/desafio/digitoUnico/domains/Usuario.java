package com.desafio.digitoUnico.domains;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name ="tbn_usuario")
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Usuario {

    @Id
    @ApiModelProperty(value = "Id da entidade Usuario")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id" )
    @Column(name =  "id", nullable = false)
    private Long id;

    @NotBlank
    @ApiModelProperty(value = "Nome do Usuário")
    @Size(min = 2, max = 1000)
    @Column(name =  "nome", nullable = false)
    private String nome;

    @NotBlank
    @ApiModelProperty(value = "E-mail do Usuário")
    @Size(min = 1, max = 1000)
    @Column(name = "email", nullable = false)
    private String email;

    @ApiModelProperty(value = "Dígitos gerados pelo Usuário")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usuario", targetEntity = DigitoUnico.class)
    @JsonManagedReference
    private Set<DigitoUnico> digitos = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeUsuarrio() {
        return nome;
    }

    public void setNomeUsuarrio(String nomeUsuarrio) {
        this.nome = nomeUsuarrio;
    }

    public String getEmailUsuario() {
        return email;
    }

    public void setEmailUsuario(String emailUsuario) {
        this.email = emailUsuario;
    }

    public Set<DigitoUnico> getDigitos() {
        return digitos;
    }

    public void setDigitos(Set<DigitoUnico> digitos) {
        this.digitos = digitos;
    }
}
