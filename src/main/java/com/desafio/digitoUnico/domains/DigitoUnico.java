package com.desafio.digitoUnico.domains;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.Min;

@Entity
@Table(name = "tbn_digitoUnico")
@Getter @Setter @NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DigitoUnico  {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name ="digito", nullable = false)
    private String digito;

    @Min(0)
    @Column(name ="numeroConcatenacao", nullable = true)
    private Integer numeroConcatenacao;

    @Min(1)
    @Column(name ="numeroEncontrado", nullable = false)
    private Integer numeroEncontrado;

    @JoinColumn(name = "IDusuario", nullable = true, foreignKey = @ForeignKey(name = "digitoUnico_usuario"))
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private Usuario usuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDigito() {
        return digito;
    }

    public void setDigito(String digito) {
        this.digito = digito;
    }

    public Integer getNumeroConcatenacao() {
        return numeroConcatenacao;
    }

    public void setNumeroConcatenacao(Integer numeroConcatenacao) {
        this.numeroConcatenacao = numeroConcatenacao;
    }

    public Integer getNumeroEncontrado() {
        return numeroEncontrado;
    }

    public void setNumeroEncontrado(Integer numeroEncontrado) {
        this.numeroEncontrado = numeroEncontrado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
