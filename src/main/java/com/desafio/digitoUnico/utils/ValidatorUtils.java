package com.desafio.digitoUnico.utils;

import com.desafio.digitoUnico.dtos.DigitoUnicoCalculoDto;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ValidatorUtils {

    public static ResponseEntity<String> validarParametros(DigitoUnicoCalculoDto dto) {
        List<String> erros = listarErrosParam(dto);
        if (!CollectionUtils.isEmpty(erros)) {
            String body = "";
            erros.forEach(body::concat);
            return ResponseEntity.badRequest().body(body);
        }
        return ResponseEntity.ok().body("OK");
    }

    public static List<String> listarErrosParam(DigitoUnicoCalculoDto dto) {
        List<String> list = new ArrayList<>();

        if (Objects.isNull(dto.getDigito())) {
            list.add("O dígito não pode ser nulo!");
        }

        BigInteger concatMax = BigInteger.TEN.pow(5);
        if (Objects.nonNull(dto.getNumeroConcatenacao()) && (dto.getNumeroConcatenacao() < 1
                || (concatMax.compareTo(BigInteger.valueOf(dto.getNumeroConcatenacao())) < 0))) {
            list.add("Concatenação excede o máximo(10^5).");
        }

        BigInteger tamanhoMax = BigInteger.TEN.pow(1000000);
        if ((new BigInteger(dto.getDigito()).compareTo(BigInteger.ONE) < 0)
                || (tamanhoMax.compareTo(new BigInteger(dto.getDigito())) < 0)) {
            list.add("O dígito informado excede o máximo(10^^1000000).");
        }
        return list;
    }

    public static ResponseEntity<String> validarCriptografiaUsuario(Long id, boolean isCriptografia) {
        List<String> erros = listarErrosCriptografia(id, isCriptografia);
        if (!CollectionUtils.isEmpty(erros)) {
            String body = "";
            for (String string : erros) {
                body = body.concat(" ").concat(string);
            }
            return ResponseEntity.badRequest().body(body);
        }
        return ResponseEntity.ok().body("OK");
    }

    private static List<String> listarErrosCriptografia(Long id, boolean isCriptografia) {
        List<String> list = new ArrayList<>();
        if (Objects.isNull(id)) {
            list.add("O id usuário não pode ser nulo!");
        }

        if ((isCriptografia && CriptografiaUtils.usuarioPossuiChave(id))) {
            list.add("O usuário já foi criptografado!");
        }
        if ((!isCriptografia && !CriptografiaUtils.usuarioPossuiChave(id))) {
            list.add("O usuário ainda não foi criptografado!");
        }
        return list;
    }
}
