package com.desafio.digitoUnico.utils;

import com.desafio.digitoUnico.constante.Contantante;
import com.desafio.digitoUnico.domains.Usuario;
import lombok.extern.slf4j.Slf4j;
import javax.crypto.Cipher;
import javax.management.openmbean.InvalidKeyException;
import java.security.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class CriptografiaUtils {
    private static final String UTF8 = Contantante.UTF8;
    private static final String ALGORITMO_RSA = Contantante.ALGORITMO_RSA;
    private static final String GENERAL_ERROR =Contantante.GENERAL_ERROR;
    private static final String GERAR_CHAVES_ERROR = Contantante.GERAR_CHAVES_ERROR;
    private static final Map<Long, KeyPair> keys = new HashMap<>();
    private static final Base64.Decoder DECODER = Base64.getDecoder();
    private static final Base64.Encoder ENCODER = Base64.getEncoder();

    public static void gerarChaves(Long idUsuario) throws GeneralSecurityException {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITMO_RSA);
            keyGen.initialize(1024);
            KeyPair keyPair = keyGen.generateKeyPair();
            keys.put(idUsuario, keyPair);
        } catch (Exception e) {
            throw new GeneralSecurityException(GERAR_CHAVES_ERROR);
        }
    }

    public static Usuario criptografar(Usuario usuario) throws GeneralSecurityException {
        gerarChaves(usuario.getId());
        PublicKey chavePublica = keys.get(usuario.getId()).getPublic();
        usuario.setNomeUsuarrio(criptografarTexto(usuario.getNomeUsuarrio(), chavePublica));
        usuario.setEmailUsuario(criptografarTexto(usuario.getEmailUsuario(), chavePublica));
        return usuario;
    }

    private static String criptografarTexto(String texto, PublicKey chavePublica) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITMO_RSA);
            cipher.init(Cipher.ENCRYPT_MODE, chavePublica);
            byte[] textoBytes = cipher.doFinal(texto.getBytes());
            return ENCODER.encodeToString(textoBytes);
        } catch (Exception e) {
            throw new InvalidKeyException(GENERAL_ERROR);
        }
    }

    public static Usuario descriptografar(Usuario usuario) {
        PrivateKey chavePrivada = keys.get(usuario.getId()).getPrivate();
        usuario.setNomeUsuarrio(descriptografarTexto(usuario.getNomeUsuarrio(), chavePrivada));
        usuario.setEmailUsuario(descriptografarTexto(usuario.getEmailUsuario(), chavePrivada));
        keys.remove(usuario.getId());
        return usuario;
    }

    private static String descriptografarTexto(String textoCriptografado, PrivateKey chavePrivada) {
        try {
            Cipher cipher = Cipher.getInstance(ALGORITMO_RSA);
            cipher.init(Cipher.DECRYPT_MODE, chavePrivada);
            byte[] byteDecode = DECODER.decode(textoCriptografado);
            return new String(cipher.doFinal(byteDecode), UTF8);
        } catch (Exception e) {
            throw new InvalidKeyException(GENERAL_ERROR);
        }
    }

    public static boolean usuarioPossuiChave(Long idUsuario) {
        return !keys.isEmpty() && keys.containsKey(idUsuario);
    }
}
