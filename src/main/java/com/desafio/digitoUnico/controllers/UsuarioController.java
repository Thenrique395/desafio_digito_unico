package com.desafio.digitoUnico.controllers;

import com.desafio.digitoUnico.domains.Usuario;
import com.desafio.digitoUnico.dtos.UsuarioDto;
import com.desafio.digitoUnico.service.UsuarioService;
import com.desafio.digitoUnico.utils.ValidatorUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.security.GeneralSecurityException;


@RestController
@Api("Usuario")
@RequestMapping("/api/usuario")
public class UsuarioController {

    private UsuarioService usuarioService;

    @Autowired
    public  UsuarioController(UsuarioService usuarioService){
        this.usuarioService = usuarioService;
    }

    @PostMapping("/cadastrar")
    @ApiOperation(httpMethod = "POST", value = "Cadastra usuário na base de dados")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Usuário cadastrado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao cadastrar usuário") })
    public ResponseEntity<UsuarioDto>  cadastrar( @RequestBody UsuarioDto usuarioDto){
        UsuarioDto uduarioDtoCadastrado = usuarioService.cadastrar(usuarioDto);

        return  new ResponseEntity(uduarioDtoCadastrado, HttpStatus.CREATED);
    }

    @GetMapping("/getById/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "GET", value = "Buscar usuário pelo seu código de identificação")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Usuário encontrado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao encontrar usuário") })
    public ResponseEntity<UsuarioDto>  getById( @PathVariable(value = "id") Long id){

        UsuarioDto usuarioDto = usuarioService.getById(id);

        return  new ResponseEntity(usuarioDto, HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "DELETE", value = "Deletando usuário na base de dados.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Usuário deletado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao deletar usuário") })
    public ResponseEntity<UsuarioDto>  remover( @PathVariable(value = "id") Long id){
        UsuarioDto usuarioDto = usuarioService.delete(id);
        return  new ResponseEntity(usuarioDto, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(httpMethod = "PUT", value = "Atualiza usuário na base de dados.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Usuário atualizado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao atualizar usuário") })
    public ResponseEntity<UsuarioDto>  atualizar( @RequestBody UsuarioDto usuarioDto){

       usuarioService.atualizar(usuarioDto);

        return  new ResponseEntity(usuarioDto, HttpStatus.CREATED);
    }

    @PutMapping("/criptografar/{id}")
    @ApiOperation(httpMethod = "PUT", value = "Criptografar os atributos do usuário.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Usuário criptografado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao criptografar usuário") })
    public ResponseEntity<Usuario> criptografarUsuario(@PathVariable(value = "id", required = true) Long id) throws GeneralSecurityException {
        ResponseEntity<String> response = ValidatorUtils.validarCriptografiaUsuario(id, Boolean.TRUE);
        if (response.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
            throw new ResponseStatusException(response.getStatusCode(), response.getBody());
        }
        UsuarioDto dto = new UsuarioDto();
        dto = usuarioService.criptografar(id);
        return new ResponseEntity(dto, HttpStatus.OK);
    }

    @PutMapping("/descriptografar/{id}")
    @ApiOperation(httpMethod = "PUT",
            value = "Descriptografar os atributos do usuário.",
            nickname = "descriptografar",
            tags = { "usuario", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Usuário descriptografado com sucesso!"),
            @ApiResponse(code = 400, message = "Falha ao descriptografar usuário")})
    public ResponseEntity<UsuarioDto> descriptografarDadosUsuario(
            @PathVariable(value = "id", required = true) Long id) {
        ResponseEntity<String> response = ValidatorUtils.validarCriptografiaUsuario(id, Boolean.FALSE);
        if (response.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
            throw new ResponseStatusException(response.getStatusCode(), response.getBody());
        }

        UsuarioDto dto = new UsuarioDto();
        dto = usuarioService.descriptografar(id);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}
