package com.desafio.digitoUnico.constante;

import java.security.KeyPair;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class Contantante {

    public static final String UTF8 = "UTF8";
    public static final String ALGORITMO_RSA = "RSA";
    public static final String GENERAL_ERROR = "Texto muito longo ou chave inválida";
    public static final String GERAR_CHAVES_ERROR = "Falha ao gerar chaves para criptografia";
    public static final Map<Long, KeyPair> keys = new HashMap<>();
    public static final Base64.Decoder DECODER = Base64.getDecoder();
    public static final Base64.Encoder ENCODER = Base64.getEncoder();
}
