package com.desafio.digitoUnico.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(true)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Desafio Banco Inter : Dígito Único")
                .description("Se x tem apenas um dígito, então o seu dígito único é x."
                        +" Caso contrário, o dígito único de x é igual ao dígito único da"
                        +" soma dos dígitos de x.")
                .version("1.0.0")
                .contact(  contact()).build();
    }

    private springfox.documentation.service.Contact contact(){
        return  new springfox.documentation.service.Contact("Tiago Henrique dos Santos",
                "https://github.com/Thenrique395/CastGroup",
                "thenrique395@gmail.com");
    }
}